package com.company;

public class Human {
    private String name;
    private String surname;
    private String middleName;

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    public Human(String name, String surname, String middleName) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFullName(){
        String result = getSurname()+" "+getName();
        if (middleName != null){
            result+=getMiddleName();
        }
        return result;
    }

    public String getShortName(){
        String result = getSurname()+" "+getName().charAt(0)+".";
        if (middleName!=null && middleName.length()>0){
            result+=" "+getMiddleName().charAt(0)+".";
        }
        return result;
    }
}
