package com.company;

import java.util.Scanner;

public class Scan {
    private Scanner scanner;
    private String param;
    private boolean required;

    public Scan(Scanner scanner, String param, boolean required) {
        this.scanner = scanner;
        this.param = param;
        this.required = required;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public boolean isNotRequired() {
        return !required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
    public String scan(){
        String result;
        if (isNotRequired()){
            return setNextLine();
        }
        for (;;){
            result = setNextLine();
            if (result.length()<=0){
                System.err.println("Параметр обязательный повторите ещё раз");
                continue;
            }
            break;
        }
        return result;
    }
    private String setNextLine(){
        String message = "Введите значение " + param;
        if (isNotRequired()){
            message += " "+"(не обязательно)";
        }
        System.out.println(message+" :");

        return scanner.nextLine();
    }
}
